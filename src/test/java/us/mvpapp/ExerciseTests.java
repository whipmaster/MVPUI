package us.mvpapp;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import us.mvpapp.equipment.Equipment;
import us.mvpapp.equipment.EquipmentService;
import us.mvpapp.exercise.Exercise;
import us.mvpapp.exercise.ExerciseService;

/**
 * Tests Exercise API.
 * 
 * See https://docs.spring.io/spring/docs/current/spring-framework-reference/testing.html
 * 
 * @author James Chrystal
 *
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
@TestPropertySource("/test.yml")
public class ExerciseTests {
	@Autowired private EquipmentService equipmentService;
	@Autowired private ExerciseService exerciseService;
	
	@Before
	public void setUpTestData() {
		equipmentService.save(new Equipment("Barbell", null));
		equipmentService.save(new Equipment("Dumbell", null));
		equipmentService.save(new Equipment("Rope", null));
	}	
	
	@Test
	@Transactional
	public void testEquipmentAssociations() {
		Exercise exercise = new Exercise();
		
		// create and save exercise
		exercise.setName("Bench Press");
//		equipmentService.findAll().forEach(exercise.getPreferredEquipment()::add);
//		equipmentService.findAll().forEach(exercise.getAlternateEquipment()::add);
		exerciseService.save(exercise);
		
		// fetch new instance of same entity and verify associations
		exercise = exerciseService.findOne(exercise.getId());
		assertNotNull("exercise == null", exercise);
//		assertTrue("Did the exercise have 3 items", exercise.getPreferredEquipment().size() == 3);
		
		// remove associations
//		exercise.getPreferredEquipment().clear();
//		exercise.getAlternateEquipment().clear();
		exerciseService.save(exercise);
		
		// fetch new instance of same entity
		exercise = exerciseService.findOne(exercise.getId());
		
		// verify removing associations works and doesn't delete equipment entities
//		assertTrue("preferred equipment is empty", exercise.getPreferredEquipment().isEmpty());
//		assertTrue("alternate equipment is empty", exercise.getAlternateEquipment().isEmpty());
		assertTrue("three instances of equipment", equipmentService.findAll().spliterator().getExactSizeIfKnown() == 3); 
	}
	
	@After
	public void tearDown() {		
	}
}
