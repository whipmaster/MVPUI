package us.chrystal.api.app;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import us.chrystal.api.AbstractPersistable;
import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.Service;

/**
 * 
 * @author James.Chrystal
 *
 * @param <T>
 * @param <R>
 */
@Repository
@Transactional(readOnly = true)
public abstract class AbstractServiceImpl<T extends AbstractPersistable, R extends AbstractPersistableRepository<T>> implements Service<T> {
	private R repository;

	/**
	 * 
	 * @param repository
	 */
	protected AbstractServiceImpl(R repository) {
		this.repository = repository;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public T createOne() {
		try {			
			Class<T> instanceType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
			return instanceType.newInstance();
		} catch(Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.gafg.api.Service#findOne(java.lang.Long)
	 */
	public T findOne(Long id) {
		return repository.findOne(id);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.gafg.api.Service#findAll()
	 */
	@Override
	public List<T> findAll() {
		return repository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.gafg.api.Service#save(com.gafg.api.AbstractPersistable)
	 */
	@Override
	@Transactional
	public T save(T entity) {
		return repository.save(entity);
	}

	/*
	 * (non-Javadoc)
	 * @see com.gafg.api.Service#delete(com.gafg.api.AbstractPersistable)
	 */
	@Override
	@Transactional
	public void delete(T entity) {
		repository.delete(entity);
	}

	/**
	 * 
	 * @return
	 */
	public R getRepository() {
		return repository;
	}

}
