package us.chrystal.api;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * 
 * @author James Chrystal
 *
 * @param <T>
 */
public interface AbstractPersistableRepository<T extends AbstractPersistable> extends CrudRepository<T, Long> {
	/*
	 * (non-Javadoc)
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	@Override
	List<T> findAll();
}
