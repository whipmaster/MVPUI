package us.chrystal.api.web;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import us.chrystal.api.AbstractPersistable;
import us.chrystal.api.Service;

/**
 * 
 * @author James.Chrystal
 *
 */
public class AbstractView<T extends AbstractPersistable, S extends Service<T>, H extends AbstractPersistableHolder<T>> implements Serializable {
	private static final long serialVersionUID = 1L;
	private S service;
	private T current;
	private H holder;
	private boolean listVisible = true;
	private boolean viewVisible = false;
	private boolean editVisible = false;
	private boolean deleteVisible = false;

	public void reset() {
		getHolder().setCurrent(null);
		setCurrent(null);
		onList();
	}
	
	protected AbstractView(S service, H holder) {
		this.service = service;
		this.holder = holder;
		onList();
	}

	public void onList() {
		listVisible = true;
		viewVisible = false;
		editVisible = false;
		deleteVisible = false;
	}

	public void onAdd() {
		setCurrent(getService().createOne());
		onEdit();
	}

	public void onView() {
		listVisible = false;
		viewVisible = true;
		editVisible = false;
	}

	public void onEdit() {
		listVisible = false;
		viewVisible = false;
		editVisible = true;
	}

	public void onSave() {
		if (getCurrent() != null) {
			try {
				setCurrent(getService().save(getCurrent()));
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Item saved.", "Item saved."));
				onList();
			} catch(Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage(), e.getMessage()));
				return;
			}
		}
	}

	public void onDelete() {
		if (getCurrent() != null) {
			try {
				getService().delete(getCurrent());
				setCurrent(null);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Item deleted.", "Item deleted."));
				onList();
			} catch(Exception e) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage(), e.getMessage()));
				return;
			}
		}
	}
    
	public Iterable<T> getAll() {
		return getService().findAll();
	}

	public boolean isListVisible() {
		return listVisible;
	}

	public void setListVisible(boolean listVisible) {
		this.listVisible = listVisible;
	}

	public boolean isViewVisible() {
		return viewVisible;
	}

	public void setViewVisible(boolean viewVisible) {
		this.viewVisible = viewVisible;
	}

	public boolean isEditVisible() {
		return editVisible;
	}

	public void setEditVisible(boolean editVisible) {
		this.editVisible = editVisible;
	}

	public boolean isDeleteVisible() {
		return deleteVisible;
	}

	public void setDeleteVisible(boolean deleteVisible) {
		this.deleteVisible = deleteVisible;
	}

	public S getService() {
		return service;
	}

	public T getCurrent() {
		if (current != null) {
			if (current != getHolder().getCurrent()) {
				if (current.getId() != null) {
					current = getService().findOne(current.getId());
				}
				
				getHolder().setCurrent(current);
			}			
		}

		return current;
	}

	public void setCurrent(T current) {
		// if current is null or empty, we accept as is
		if (current == null || current.getId() == null) {
			holder.setCurrent(current);		
		// otherwise, ensure we have a current, attached instance
		} else if (current != getHolder().getCurrent()) {
			holder.setCurrent(getService().findOne(current.getId()));
		}
		
		this.current = holder.getCurrent();
	}

	public H getHolder() {
		return holder;
	}
}
