package us.chrystal.api.web;

import us.chrystal.api.AbstractPersistable;

/**
 * 
 * @author James.Chrystal
 *
 * @param <T>
 * @param <ID>
 */
public abstract class AbstractPersistableHolder<T extends AbstractPersistable> {
	T current;

	public T getCurrent() {
		return current;
	}

	public void setCurrent(T current) {
		this.current = current;
	}
}
