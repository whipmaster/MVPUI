package us.chrystal.api.web;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.springframework.data.domain.Persistable;
import org.springframework.data.repository.CrudRepository;

/**
 * 
 * @author james.chrystal
 *
 * @param <T>
 */
public abstract class AbstractEnumConverter<T extends Persistable<Long>> implements Converter {
	private CrudRepository<T, Long> repository;
	
	public AbstractEnumConverter(CrudRepository<T, Long> repository) {
		this.repository = repository;
	}

    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
                return repository.findOne(Long.parseLong(value));
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid entity."));
            }
        }
        else {
            return null;
        }
    }
 
    @SuppressWarnings("unchecked")
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return String.valueOf(((T) object).getId());
        }
        else {
            return null;
        }
    }	
}
