package us.chrystal.api;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * Based upon Spring by Example.
 * 
 * @author james.chrystal
 *
 * @param <ID>
 */
@MappedSuperclass
@SuppressWarnings("serial")
public abstract class AbstractAuditableEntity extends AbstractPersistable {
	@LastModifiedDate
	@Column(name = "MODIFIED_DATE")
	private Date lastUpdated;
	@LastModifiedBy
	@Column(name = "MODIFIED_BY")
	private String lastUpdateUser;
	@CreatedDate
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createUser;

	public String getCreatedBy() {
		return createUser;
	}

	public void setCreatedBy(String createdBy) {
		this.createUser = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return lastUpdateUser;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastUpdateUser = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastUpdated;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastUpdated = lastModifiedDate;
	}

}
