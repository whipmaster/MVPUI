package us.chrystal.api;

import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author james.chrystal
 *
 * @param <T>
 */
public interface Service<T extends AbstractPersistable> {
	/**
	 * Convenience method for creating entity instance.
	 * 
	 * @return
	 */
	public T createOne();
	
	/**
	 * Pass-through to repository.
	 * 
	 * @return T
	 */	
	@Transactional(readOnly = true)
	T findOne(Long id);
			
	/**
	 * Pass-through to repository.
	 * 
	 * @return
	 */
	@Transactional(readOnly = true)
	Iterable<T> findAll();
	
	/**
	 * Pass-through to repository.
	 * 
	 * @param entity
	 * @return
	 */
	@Transactional
	T save(T entity);
	
	/**
	 * Pass-through to repository.
	 * 
	 * @param entity
	 */
	@Transactional(readOnly = true)
	void delete(T entity);
}
