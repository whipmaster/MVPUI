package us.mvpapp.mobility.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.mobility.Mobility;


public interface MobilityRepository extends AbstractPersistableRepository<Mobility> {

}
