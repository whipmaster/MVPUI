package us.mvpapp.mobility.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.mobility.Mobility;
import us.mvpapp.mobility.MobilityService;



@Component
public class MobilityServiceImpl extends AbstractServiceImpl<Mobility, AbstractPersistableRepository<Mobility>> implements MobilityService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected MobilityServiceImpl(MobilityRepository repository) {
		super(repository);
	}


}
