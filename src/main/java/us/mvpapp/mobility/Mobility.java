package us.mvpapp.mobility;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.athlete.Athlete;

@Entity
public class Mobility extends AbstractPersistable{

	private enum MobilityTypes
	{
		Beginner, Intermediate, Advanced
	};
	private String hip;
	private String torso;
	private String shoulder;
	private String ankle;
	private String neck;
	private String back;
	private String squat;
	private String clean;
	private String jerk;
	private String snatch;
	private String core;

	@OneToOne
	private Athlete athlete;
	

	public String getHip() {
		return hip;
	}
	public void setHip(String hip) {
		this.hip = hip;
	}
	public String getTorso() {
		return torso;
	}
	public void setTorso(String torso) {
		this.torso = torso;
	}
	public String getShoulder() {
		return shoulder;
	}
	public void setShoulder(String shoulder) {
		this.shoulder = shoulder;
	}
	public String getAnkle() {
		return ankle;
	}
	public void setAnkle(String ankle) {
		this.ankle = ankle;
	}
	public String getNeck() {
		return neck;
	}
	public void setNeck(String neck) {
		this.neck = neck;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}

	public String getSquat() {
		return squat;
	}
	public void setSquat(String squat) {
		this.squat = squat;
	}
	public String getClean() {
		return clean;
	}
	public void setClean(String clean) {
		this.clean = clean;
	}
	public String getJerk() {
		return jerk;
	}
	public void setJerk(String jerk) {
		this.jerk = jerk;
	}
	public String getSnatch() {
		return snatch;
	}
	public void setSnatch(String snatch) {
		this.snatch = snatch;
	}
	public String getCore() {
		return core;
	}
	public void setCore(String core) {
		this.core = core;
	}
	public Athlete getAthlete() {
		return athlete;
	}
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}

	
	
}
