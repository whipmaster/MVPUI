package us.mvpapp.mobility.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.mobility.Mobility;


@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MobilityHolder extends AbstractPersistableHolder<Mobility> {
	
}
