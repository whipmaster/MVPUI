package us.mvpapp.mobility.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.mobility.Mobility;
import us.mvpapp.mobility.MobilityService;







@Component
@Scope("view")
public class MobilityView extends AbstractView<Mobility, MobilityService, MobilityHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected MobilityView(MobilityService service, MobilityHolder holder) {
		super(service, holder);
	}
}
