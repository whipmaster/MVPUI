package us.mvpapp;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.stereotype.Component;

/**
 * Replaces web.xml.
 * 
 * @author james.chrystal
 *
 */
@Configuration
public class WebAppInitializer implements ServletContextInitializer {
		
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
//		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();		
//		rootContext.register(AppConfig.class);
//		rootContext.register(SecurityConfig.class);
//		
//		servletContext.addListener(new ContextLoaderListener(rootContext));
//	
		System.out.println("##### Registering OSIVF #####");
		OpenEntityManagerInViewFilter openSessionInViewFilter = new OpenEntityManagerInViewFilter();
		FilterRegistration.Dynamic openSessionInViewFilterRegistration = servletContext.addFilter("characterEncoding", openSessionInViewFilter);
		openSessionInViewFilterRegistration.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.ERROR), true, "*.jsf");

//		configureSpringMvc(servletContext, rootContext);
	}

	@Component
	public class ServletCustomizer implements EmbeddedServletContainerCustomizer {
	    @Override
	    public void customize(ConfigurableEmbeddedServletContainer container) {
	        MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
	        mappings.add("ico","image/vnd.microsoft.icon");
	        container.setMimeMappings(mappings);
	    }
	}

	/**
	 * Needed for multipart form uploads.
	 *  
	 * @return
	 */
    private MultipartConfigElement getMultipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("128MB");
        factory.setMaxRequestSize("128MB");
        return factory.createMultipartConfig();
    }	
}
