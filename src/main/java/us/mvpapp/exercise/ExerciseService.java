package us.mvpapp.exercise;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface ExerciseService extends Service<Exercise>{
}
