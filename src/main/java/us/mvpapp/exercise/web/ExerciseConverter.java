package us.mvpapp.exercise.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableConverter;
import us.mvpapp.exercise.Exercise;
import us.mvpapp.exercise.app.ExerciseRepository;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class ExerciseConverter extends AbstractPersistableConverter<Exercise> {

	@Autowired
	public ExerciseConverter(ExerciseRepository repository) {
		super(repository);
	}
}
