package us.mvpapp.exercise.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.equipment.Equipment;
import us.mvpapp.equipment.EquipmentService;
import us.mvpapp.exercise.Exercise;
import us.mvpapp.exercise.ExerciseService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class ExerciseView extends AbstractView<Exercise, ExerciseService, ExerciseHolder> {
	private static final long serialVersionUID = 1L;
	private EquipmentService equipmentService;
	
	@Autowired
	protected ExerciseView(ExerciseService service, ExerciseHolder holder) {
		super(service, holder);
	}
		
	public Iterable<Equipment> getEquipment() {
		return equipmentService.findAll();
	}	
}
