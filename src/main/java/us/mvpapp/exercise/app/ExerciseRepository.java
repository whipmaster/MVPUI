package us.mvpapp.exercise.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.exercise.Exercise;

/**
 * 
 * @author james.chrystal
 *
 */
public interface ExerciseRepository extends AbstractPersistableRepository<Exercise> {
}
