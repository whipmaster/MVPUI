package us.mvpapp.exercise.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.exercise.Exercise;
import us.mvpapp.exercise.ExerciseService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class ExerciseServiceImpl extends AbstractServiceImpl<Exercise, ExerciseRepository> implements ExerciseService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected ExerciseServiceImpl(ExerciseRepository repository) {
		super(repository);
	}

}
