package us.mvpapp.exercise;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.equipment.Equipment;
import us.mvpapp.uom.UnitOfMeasure;

/**
 * Represents a piece of gym equipment, e.g., Barbell, Squat Rack, etc.
 * 
 * @author James Chrystal
 *
 */
@Entity
public class Exercise extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private String testable;
 	@ManyToOne
 	@JoinTable(name="exercise_unitofmeasure", joinColumns = @JoinColumn(name = "exercise_id"), inverseJoinColumns = @JoinColumn(name = "unitofmeasure_id"))	
	private UnitOfMeasure unitOfMeasure;
	
 	@ManyToMany
 	@JoinTable(name="exercise_equipment", joinColumns = @JoinColumn(name = "exercise_id"), inverseJoinColumns = @JoinColumn(name = "equipment_id"))
	private List<Equipment> equipment = new ArrayList<>();

	public List<Equipment> getEquipment() {
		return equipment;
	}

	public void setEquipment(List<Equipment> equipment) {
		this.equipment = equipment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTestable() {
		return testable;
	}

	public void setTestable(String testable) {
		this.testable = testable;
	}

	public UnitOfMeasure getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

}
