package us.mvpapp.workoutdetail;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.exercise.Exercise;

/**
 * Represents a single exercise in a workout
 * 
 * @author James Chrystal
 *
 */
@Entity
public class WorkoutDetail extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinTable(name = "workoutdetail_exercise", joinColumns = @JoinColumn(name = "workoutdetail_id"), inverseJoinColumns = @JoinColumn(name = "exercise_id"))
	private Exercise exercise;
	private String optionalFlag;


	public String getOptionalFlag() {
		return optionalFlag;
	}

	public void setOptionalFlag(String optionalFlag) {
		this.optionalFlag = optionalFlag;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}

}
