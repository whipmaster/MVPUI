package us.mvpapp.workoutdetail.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.workoutdetail.WorkoutDetail;

/**
 * 
 * @author james.chrystal
 *
 */
public interface WorkoutDetailRepository extends AbstractPersistableRepository<WorkoutDetail> {
}
