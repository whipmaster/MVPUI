package us.mvpapp.workoutdetail.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.workoutdetail.WorkoutDetail;
import us.mvpapp.workoutdetail.WorkoutDetailService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class WorkoutDetailServiceImpl extends AbstractServiceImpl<WorkoutDetail, WorkoutDetailRepository> implements WorkoutDetailService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected WorkoutDetailServiceImpl(WorkoutDetailRepository repository) {
		super(repository);
	}

}
