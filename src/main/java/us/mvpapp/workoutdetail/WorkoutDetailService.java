package us.mvpapp.workoutdetail;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface WorkoutDetailService extends Service<WorkoutDetail>{
}
