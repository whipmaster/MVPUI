package us.mvpapp.workoutdetail.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.workout.Workout;
import us.mvpapp.workout.WorkoutDetails;
import us.mvpapp.workout.web.WorkoutView;
import us.mvpapp.workoutdetail.WorkoutDetail;
import us.mvpapp.workoutdetail.WorkoutDetailService;
import us.mvpapp.workoutphase.WorkoutPhase;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class WorkoutDetailView extends AbstractView<WorkoutDetail, WorkoutDetailService, WorkoutDetailHolder> {
	private static final long serialVersionUID = 1L;
	@Autowired WorkoutView workoutView;
	private WorkoutPhase workoutPhase;

	@Autowired
	protected WorkoutDetailView(WorkoutDetailService service, WorkoutDetailHolder holder) {
		super(service, holder);
	}

	public WorkoutPhase getWorkoutPhase() {
		return workoutPhase;
	}

	public void setWorkoutPhase(WorkoutPhase workoutPhase) {
		this.workoutPhase = workoutPhase;
	}

	/*
	 * (non-Javadoc)
	 * @see us.chrystal.api.web.AbstractView#onSave()
	 */
	@Override
	public void onSave() {
		Workout workout = workoutView.getCurrent();
		WorkoutDetails workoutDetails = workout.getWorkoutDetails().get(workoutPhase);
		if (!workoutDetails.getWorkoutDetails().contains(getCurrent())) {
			workoutDetails.getWorkoutDetails().add(getCurrent());
		}
		
		super.onSave();
	}

	@Override
	public void onDelete() {
		Workout workout = workoutView.getCurrent();
		WorkoutDetails workoutDetails = workout.getWorkoutDetails().get(workoutPhase);
		workoutDetails.getWorkoutDetails().remove(getCurrent());
		super.onSave();
	}
}
