package us.mvpapp.workoutdetail.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.workoutdetail.WorkoutDetail;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WorkoutDetailHolder extends AbstractPersistableHolder<WorkoutDetail> {
}
