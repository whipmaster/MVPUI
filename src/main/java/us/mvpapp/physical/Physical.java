package us.mvpapp.physical;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.athlete.Athlete;

@Entity
public class Physical extends AbstractPersistable{

	private String height;
	private String weight;
	private String shirtSize;
	private String pantSize;
	private String shoeSize;
	private String helmetSize;
	private String gloveSize;
	@OneToOne
	private Athlete athlete;
	
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getShirtSize() {
		return shirtSize;
	}
	public void setShirtSize(String shirtSize) {
		this.shirtSize = shirtSize;
	}
	public String getPantSize() {
		return pantSize;
	}
	public void setPantSize(String pantSize) {
		this.pantSize = pantSize;
	}
	public String getShoeSize() {
		return shoeSize;
	}
	public void setShoeSize(String shoeSize) {
		this.shoeSize = shoeSize;
	}
	public String getHelmetSize() {
		return helmetSize;
	}
	public void setHelmetSize(String helmetSize) {
		this.helmetSize = helmetSize;
	}
	public String getGloveSize() {
		return gloveSize;
	}
	public void setGloveSize(String gloveSize) {
		this.gloveSize = gloveSize;
	}
	public Athlete getAthlete() {
		return athlete;
	}
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}

	
	
}
