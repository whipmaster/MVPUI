package us.mvpapp.physical.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.physical.Physical;
import us.mvpapp.physical.PhysicalService;





@Component
@Scope("view")
public class PhysicalView extends AbstractView<Physical, PhysicalService, PhysicalHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected PhysicalView(PhysicalService service, PhysicalHolder holder) {
		super(service, holder);
	}
}
