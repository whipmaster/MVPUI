package us.mvpapp.physical.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.physical.Physical;
import us.mvpapp.physical.PhysicalService;


@Component
public class PhysicalServiceImpl extends AbstractServiceImpl<Physical, AbstractPersistableRepository<Physical>> implements PhysicalService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected PhysicalServiceImpl(PhysicalRepository repository) {
		super(repository);
	}


}
