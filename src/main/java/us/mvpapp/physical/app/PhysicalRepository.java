package us.mvpapp.physical.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.physical.Physical;


public interface PhysicalRepository extends AbstractPersistableRepository<Physical> {

}
