package us.mvpapp.uom;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface UnitOfMeasureService extends Service<UnitOfMeasure> {
}
