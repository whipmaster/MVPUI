package us.mvpapp.uom;

import javax.persistence.Entity;

import us.chrystal.api.AbstractPersistable;

/**
 * Represents a unit of measure, e.g., mile, foot, KG, LB, etc.
 * 
 * @author James Chrystal
 *
 */
@Entity
public class UnitOfMeasure extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
