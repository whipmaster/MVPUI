package us.mvpapp.uom.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.uom.UnitOfMeasure;
import us.mvpapp.uom.UnitOfMeasureService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class UnitOfMeasureServiceImpl extends AbstractServiceImpl<UnitOfMeasure, UnitOfMeasureRepository> implements UnitOfMeasureService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected UnitOfMeasureServiceImpl(UnitOfMeasureRepository repository) {
		super(repository);
	}

}
