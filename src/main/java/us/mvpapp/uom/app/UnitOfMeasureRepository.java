package us.mvpapp.uom.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.uom.UnitOfMeasure;

/**
 * 
 * @author james.chrystal
 *
 */
public interface UnitOfMeasureRepository extends AbstractPersistableRepository<UnitOfMeasure> {
}
