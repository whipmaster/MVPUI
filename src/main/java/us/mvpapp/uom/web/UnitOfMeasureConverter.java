package us.mvpapp.uom.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableConverter;
import us.mvpapp.uom.UnitOfMeasure;
import us.mvpapp.uom.app.UnitOfMeasureRepository;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class UnitOfMeasureConverter extends AbstractPersistableConverter<UnitOfMeasure> {

	@Autowired
	public UnitOfMeasureConverter(UnitOfMeasureRepository repository) {
		super(repository);
	}
}
