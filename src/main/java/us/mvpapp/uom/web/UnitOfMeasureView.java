package us.mvpapp.uom.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.uom.UnitOfMeasure;
import us.mvpapp.uom.UnitOfMeasureService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class UnitOfMeasureView extends AbstractView<UnitOfMeasure, UnitOfMeasureService, UnitOfMeasureHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected UnitOfMeasureView(UnitOfMeasureService service, UnitOfMeasureHolder holder) {
		super(service, holder);
	}
}
