package us.mvpapp.workout.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.workout.Workout;

/**
 * 
 * @author james.chrystal
 *
 */
public interface WorkoutRepository extends AbstractPersistableRepository<Workout> {
}
