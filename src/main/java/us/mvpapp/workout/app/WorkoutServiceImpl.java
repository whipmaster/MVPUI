package us.mvpapp.workout.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.workout.Workout;
import us.mvpapp.workout.WorkoutService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class WorkoutServiceImpl extends AbstractServiceImpl<Workout, WorkoutRepository> implements WorkoutService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected WorkoutServiceImpl(WorkoutRepository repository) {
		super(repository);
	}

}
