package us.mvpapp.workout.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.workout.Workout;
import us.mvpapp.workout.WorkoutDetails;
import us.mvpapp.workout.WorkoutService;
import us.mvpapp.workoutphase.WorkoutPhase;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class WorkoutView extends AbstractView<Workout, WorkoutService, WorkoutHolder> {
	private static final long serialVersionUID = 1L;

	@Autowired
	protected WorkoutView(WorkoutService service, WorkoutHolder holder) {
		super(service, holder);
	}	
	
	public void onEditWorkoutDetail() {
		
		setEditVisible(true);
	}
	
	public void onSaveWorkoutDetail() {
		
	}

	/*
	 * (non-Javadoc)
	 * @see us.chrystal.api.web.AbstractView#onSave()
	 * 
	 * Ensure workout details exists for each workout phase
	 */
	@Override
	public void onSave() {
		for(WorkoutPhase workoutPhase : getCurrent().getWorkoutPhases()) {
			
			if (getCurrent().getWorkoutDetails().get(workoutPhase) == null) {
				getCurrent().getWorkoutDetails().put(workoutPhase, new WorkoutDetails());
			}
		}
		
		super.onSave();
	}	
}
