package us.mvpapp.workout;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.workoutdetail.WorkoutDetail;

/**
 * 
 * @author James Chrystal
 *
 */
@Entity
public class WorkoutDetails extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "workoutdetails_id")
	private List<WorkoutDetail> workoutDetails = new ArrayList<>();

	public List<WorkoutDetail> getWorkoutDetails() {
		return workoutDetails;
	}

	public void setWorkoutDetails(List<WorkoutDetail> workoutDetails) {
		this.workoutDetails = workoutDetails;
	}
}
