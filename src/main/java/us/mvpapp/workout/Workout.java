package us.mvpapp.workout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.workoutphase.WorkoutPhase;

/**
 * Represents a piece of gym equipment, e.g., Barbell, Squat Rack, etc.
 * 
 * @author James Chrystal
 *
 */
@Entity
public class Workout extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private String instructions;
	private String optionalFlag;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "workout_id")
	@MapKeyColumn(name = "workoutphase_id")
	private Map<WorkoutPhase, WorkoutDetails> workoutDetails = new HashMap<>();

	@ManyToMany
	@JoinTable(name = "workout_phase", joinColumns = @JoinColumn(name = "workout_id"), inverseJoinColumns = @JoinColumn(name = "workoutphase_id"))
	private List<WorkoutPhase> workoutPhases = new ArrayList<>();

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public String getOptionalFlag() {
		return optionalFlag;
	}

	public void setOptionalFlag(String optionalFlag) {
		this.optionalFlag = optionalFlag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<WorkoutPhase, WorkoutDetails> getWorkoutDetails() {
		return workoutDetails;
	}

	public void setWorkoutDetails(Map<WorkoutPhase, WorkoutDetails> workoutDetails) {
		this.workoutDetails = workoutDetails;
	}

	public List<WorkoutPhase> getWorkoutPhases() {
		return workoutPhases;
	}

	public void setWorkoutPhases(List<WorkoutPhase> workoutPhases) {
		this.workoutPhases = workoutPhases;
	}

}
