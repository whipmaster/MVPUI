package us.mvpapp.workout;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface WorkoutService extends Service<Workout>{
}
