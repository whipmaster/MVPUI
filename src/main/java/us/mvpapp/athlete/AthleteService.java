package us.mvpapp.athlete;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface AthleteService extends Service<Athlete> {
}
