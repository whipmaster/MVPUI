package us.mvpapp.athlete.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.athlete.Athlete;
import us.mvpapp.athlete.AthleteService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class AthleteView extends AbstractView<Athlete, AthleteService, AthleteHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected AthleteView(AthleteService service, AthleteHolder holder) {
		super(service, holder);
	}
}
