package us.mvpapp.athlete.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableConverter;
import us.mvpapp.athlete.Athlete;
import us.mvpapp.athlete.app.AthleteRepository;

@Component
public class AthleteConverter extends AbstractPersistableConverter<Athlete> {
	
	@Autowired
	public AthleteConverter(AthleteRepository repository) {
		super(repository);
	}

}
