package us.mvpapp.athlete.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.athlete.Athlete;
import us.mvpapp.athlete.AthleteService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class AthleteServiceImpl extends AbstractServiceImpl<Athlete, AbstractPersistableRepository<Athlete>> implements AthleteService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected AthleteServiceImpl(AthleteRepository repository) {
		super(repository);
	}

}
