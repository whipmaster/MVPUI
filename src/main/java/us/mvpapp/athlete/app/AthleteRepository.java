package us.mvpapp.athlete.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.athlete.Athlete;

/**
 * 
 * @author james.chrystal
 *
 */
public interface AthleteRepository extends AbstractPersistableRepository<Athlete> {
}
