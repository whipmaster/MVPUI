package us.mvpapp.workoutphase;

import javax.persistence.Entity;

import us.chrystal.api.AbstractPersistable;

/**
 * 
 * @author James Chrystal
 *
 */
@Entity
public class WorkoutPhase extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
