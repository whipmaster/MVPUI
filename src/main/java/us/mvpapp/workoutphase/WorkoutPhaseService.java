package us.mvpapp.workoutphase;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface WorkoutPhaseService extends Service<WorkoutPhase> {
}
