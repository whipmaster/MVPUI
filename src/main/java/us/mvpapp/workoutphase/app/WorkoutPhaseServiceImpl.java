package us.mvpapp.workoutphase.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.workoutphase.WorkoutPhase;
import us.mvpapp.workoutphase.WorkoutPhaseService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class WorkoutPhaseServiceImpl extends AbstractServiceImpl<WorkoutPhase, WorkoutPhaseRepository> implements WorkoutPhaseService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected WorkoutPhaseServiceImpl(WorkoutPhaseRepository repository) {
		super(repository);
	}

}
