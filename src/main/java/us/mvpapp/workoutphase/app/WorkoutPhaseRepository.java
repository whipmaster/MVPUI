package us.mvpapp.workoutphase.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.workoutphase.WorkoutPhase;

/**
 * 
 * @author james.chrystal
 *
 */
public interface WorkoutPhaseRepository extends AbstractPersistableRepository<WorkoutPhase> {
}
