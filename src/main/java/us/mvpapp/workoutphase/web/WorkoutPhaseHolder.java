package us.mvpapp.workoutphase.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.workoutphase.WorkoutPhase;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class WorkoutPhaseHolder extends AbstractPersistableHolder<WorkoutPhase> {
}
