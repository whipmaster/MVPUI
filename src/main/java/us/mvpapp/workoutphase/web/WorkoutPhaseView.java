package us.mvpapp.workoutphase.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.workoutphase.WorkoutPhase;
import us.mvpapp.workoutphase.WorkoutPhaseService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class WorkoutPhaseView extends AbstractView<WorkoutPhase, WorkoutPhaseService, WorkoutPhaseHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected WorkoutPhaseView(WorkoutPhaseService service, WorkoutPhaseHolder holder) {
		super(service, holder);
	}		
}
