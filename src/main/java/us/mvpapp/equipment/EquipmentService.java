package us.mvpapp.equipment;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface EquipmentService extends Service<Equipment>{
}
