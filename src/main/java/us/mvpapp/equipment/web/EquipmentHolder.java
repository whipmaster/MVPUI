package us.mvpapp.equipment.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.equipment.Equipment;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class EquipmentHolder extends AbstractPersistableHolder<Equipment> {
}
