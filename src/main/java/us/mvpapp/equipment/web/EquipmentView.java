package us.mvpapp.equipment.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.equipment.Equipment;
import us.mvpapp.equipment.EquipmentService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class EquipmentView extends AbstractView<Equipment, EquipmentService, EquipmentHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected EquipmentView(EquipmentService service, EquipmentHolder holder) {
		super(service, holder);
	}
}
