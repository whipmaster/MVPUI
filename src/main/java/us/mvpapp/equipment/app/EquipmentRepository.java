package us.mvpapp.equipment.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.equipment.Equipment;

/**
 * 
 * @author james.chrystal
 *
 */
public interface EquipmentRepository extends AbstractPersistableRepository<Equipment> {
}
