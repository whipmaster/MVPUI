package us.mvpapp.equipment.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.equipment.Equipment;
import us.mvpapp.equipment.EquipmentService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class EquipmentServiceImpl extends AbstractServiceImpl<Equipment, EquipmentRepository> implements EquipmentService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected EquipmentServiceImpl(EquipmentRepository repository) {
		super(repository);
	}

}
