package us.mvpapp.equipment;

import javax.persistence.Entity;

import us.chrystal.api.AbstractPersistable;

/**
 * Represents a piece of gym equipment, e.g., Barbell, Squat Rack, etc.
 * 
 * @author James Chrystal
 *
 */
@Entity
public class Equipment extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;

	public Equipment() {		
	}
	
	public Equipment(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
