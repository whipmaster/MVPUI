package us.mvpapp.parent;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.athlete.Athlete;

@Entity
public class Parent extends AbstractPersistable{

	private String firstName;
	private String middleName;
	private String lastName;
	private String occupation;
	private String emailAddress;
	private String phoneNumber;
	private String college;
	private Boolean livingWith;
	@ManyToOne
	private Athlete athlete;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCollege() {
		return college;
	}
	public void setCollege(String college) {
		this.college = college;
	}
	public Boolean getLivingWith() {
		return livingWith;
	}
	public void setLivingWith(Boolean livingWith) {
		this.livingWith = livingWith;
	}
	public Athlete getAthlete() {
		return athlete;
	}
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}

	
	
}
