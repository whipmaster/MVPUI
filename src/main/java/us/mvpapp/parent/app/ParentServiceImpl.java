package us.mvpapp.parent.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.parent.Parent;
import us.mvpapp.parent.ParentService;

@Component
public class ParentServiceImpl extends AbstractServiceImpl<Parent, AbstractPersistableRepository<Parent>> implements ParentService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected ParentServiceImpl(ParentRepository repository) {
		super(repository);
	}


}
