package us.mvpapp.parent.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.parent.Parent;

public interface ParentRepository extends AbstractPersistableRepository<Parent> {

}
