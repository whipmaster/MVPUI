package us.mvpapp.parent.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.parent.Parent;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ParentHolder extends AbstractPersistableHolder<Parent> {
	
}
