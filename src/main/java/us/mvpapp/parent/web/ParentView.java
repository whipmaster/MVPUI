package us.mvpapp.parent.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.parent.Parent;
import us.mvpapp.parent.ParentService;


@Component
@Scope("view")
public class ParentView extends AbstractView<Parent, ParentService, ParentHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected ParentView(ParentService service, ParentHolder holder) {
		super(service, holder);
	}
}
