package us.mvpapp.academic.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.academic.Academic;
import us.mvpapp.academic.AcademicService;



@Component
@Scope("view")
public class AcademicView extends AbstractView<Academic, AcademicService, AcademicHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected AcademicView(AcademicService service, AcademicHolder holder) {
		super(service, holder);
	}
}
