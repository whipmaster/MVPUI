package us.mvpapp.academic.web;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractPersistableHolder;
import us.mvpapp.academic.Academic;
import us.mvpapp.parent.Parent;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AcademicHolder extends AbstractPersistableHolder<Academic> {
	
}
