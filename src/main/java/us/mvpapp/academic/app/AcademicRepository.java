package us.mvpapp.academic.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.academic.Academic;


public interface AcademicRepository extends AbstractPersistableRepository<Academic> {

}
