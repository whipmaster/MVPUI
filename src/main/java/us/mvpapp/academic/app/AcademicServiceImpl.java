package us.mvpapp.academic.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.AbstractPersistableRepository;
import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.academic.Academic;
import us.mvpapp.academic.AcademicService;

@Component
public class AcademicServiceImpl extends AbstractServiceImpl<Academic, AbstractPersistableRepository<Academic>> implements AcademicService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected AcademicServiceImpl(AcademicRepository repository) {
		super(repository);
	}


}
