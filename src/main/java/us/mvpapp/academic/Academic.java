package us.mvpapp.academic;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import us.chrystal.api.AbstractPersistable;
import us.mvpapp.athlete.Athlete;

@Entity
public class Academic extends AbstractPersistable{

	private String advisorName;
	private String advisorPhone;
	private String advisorEmail;
	private String cumulativeGpa;
	private String major;
	private String actScore;
	private String satScore;
	private String tutorName;
	private String tutorPhone;
	private String tutorEmail;
	@OneToOne
	private Athlete athlete;
	
	public String getAdvisorName() {
		return advisorName;
	}
	public void setAdvisorName(String advisorName) {
		this.advisorName = advisorName;
	}
	public String getAdvisorPhone() {
		return advisorPhone;
	}
	public void setAdvisorPhone(String advisorPhone) {
		this.advisorPhone = advisorPhone;
	}
	public String getAdvisorEmail() {
		return advisorEmail;
	}
	public void setAdvisorEmail(String advisorEmail) {
		this.advisorEmail = advisorEmail;
	}
	public String getCumulativeGpa() {
		return cumulativeGpa;
	}
	public void setCumulativeGpa(String cumulativeGpa) {
		this.cumulativeGpa = cumulativeGpa;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getActScore() {
		return actScore;
	}
	public void setActScore(String actScore) {
		this.actScore = actScore;
	}
	public String getSatScore() {
		return satScore;
	}
	public void setSatScore(String satScore) {
		this.satScore = satScore;
	}
	public String getTutorName() {
		return tutorName;
	}
	public void setTutorName(String tutorName) {
		this.tutorName = tutorName;
	}
	public String getTutorPhone() {
		return tutorPhone;
	}
	public void setTutorPhone(String tutorPhone) {
		this.tutorPhone = tutorPhone;
	}
	public String getTutorEmail() {
		return tutorEmail;
	}
	public void setTutorEmail(String tutorEmail) {
		this.tutorEmail = tutorEmail;
	}

	
	public Athlete getAthlete() {
		return athlete;
	}
	public void setAthlete(Athlete athlete) {
		this.athlete = athlete;
	}

	
	
}
