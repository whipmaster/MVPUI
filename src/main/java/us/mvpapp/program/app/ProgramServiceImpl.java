package us.mvpapp.program.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.chrystal.api.app.AbstractServiceImpl;
import us.mvpapp.program.Program;
import us.mvpapp.program.ProgramService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
public class ProgramServiceImpl extends AbstractServiceImpl<Program, ProgramRepository> implements ProgramService {

	/**
	 * 
	 * @param repository
	 */
	@Autowired
	protected ProgramServiceImpl(ProgramRepository repository) {
		super(repository);
	}

}
