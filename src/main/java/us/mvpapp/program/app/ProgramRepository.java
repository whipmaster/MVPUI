package us.mvpapp.program.app;

import us.chrystal.api.AbstractPersistableRepository;
import us.mvpapp.program.Program;

/**
 * 
 * @author james.chrystal
 *
 */
public interface ProgramRepository extends AbstractPersistableRepository<Program> {
}
