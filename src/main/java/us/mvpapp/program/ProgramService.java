package us.mvpapp.program;

import us.chrystal.api.Service;

/**
 * 
 * @author James Chrystal
 *
 */
public interface ProgramService extends Service<Program>{
}
