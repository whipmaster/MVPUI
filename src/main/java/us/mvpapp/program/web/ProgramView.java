package us.mvpapp.program.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import us.chrystal.api.web.AbstractView;
import us.mvpapp.program.Program;
import us.mvpapp.program.ProgramService;

/**
 * 
 * @author James Chrystal
 *
 */
@Component
@Scope("view")
public class ProgramView extends AbstractView<Program, ProgramService, ProgramHolder>{
	private static final long serialVersionUID = 1L;

	@Autowired
	protected ProgramView(ProgramService service, ProgramHolder holder) {
		super(service, holder);
	}		
}
