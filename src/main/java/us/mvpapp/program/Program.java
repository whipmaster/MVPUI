package us.mvpapp.program;

import java.util.Date;

import javax.persistence.Entity;

import us.chrystal.api.AbstractPersistable;

/**
 * Represents a piece of gym equipment, e.g., Barbell, Squat Rack, etc.
 * 
 * @author James Chrystal
 *
 */
@Entity
public class Program extends AbstractPersistable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private Date startDate;
	private Date endDate;
	private String notes;
	
	

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
